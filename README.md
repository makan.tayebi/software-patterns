# Software Patterns
A summary of Design Patterns that came to use to me.
## Whiteboard Pattern
## Delegation Pattern
## Factory Pattern
## Broker Pattern
Any number of bundles are able to *use* a service, *register* a service, or *listen* for a service. OSGi uses this pattern, explained [here](https://www.osgi.org/developer/architecture/).

![services](https://www.osgi.org/wp-content/uploads/services.png)
